var vega = require('vega')
var fs = require('fs')

function visualize(nodes, startNodeId) {
    // visualize nodes in a graph using vega

    // serialize nodes to vega format
    const vegaNodes = []
    const vegaLinks = []

    let group = 0 
    nodes.forEach((neighbours, nodeId) => {       
        vegaNodes.push({
            name: nodeId,
            group: group,
            index: nodeId
        })

        neighbours.forEach(neighbourId => {
            vegaLinks.push({
                source: nodeId,
                target: neighbourId,
                value: 1
            })
        })
    })

    // check if first node has index 0, if not then shift all indexes
    if (vegaNodes[0].index !== 0) {
        vegaNodes.forEach(node => {
            node.index -= 1
        })

        vegaLinks.forEach(link => {
            link.source -= 1
            link.target -= 1
        })

        startNodeId -= 1
    }

    // change color of start node
    vegaNodes[startNodeId].group = 1


    // serialize vega data to json
    const vegaData = {
        nodes: vegaNodes,
        links: vegaLinks
    }

    // write to json file
    fs.writeFileSync('data/data.json', JSON.stringify(vegaData))
}

module.exports = {
    visualize
}