const fs = require('fs')

// wrap this into function with file name as parameter

function parseFile(path) {
    console.log(path)
    const file = fs.readFileSync(path, 'utf8', (err, data) => {
        if (err) {
            console.error(err)
            return
        }
    
        return data
    })
    
    const info = file.split('\n')
    
    // first line is the number of edges
    const N = info.shift()
    
    // rest of the lines are the edges between nodes
    const edges = info.map(line => line.split(' '))
    
    // create a map of nodes
    // use map to store relations (map is a list of key/value pairs)
    const nodes = new Map()
    
    // add nodes to the map
    edges.forEach(edge => {
        // get nodeId and neighbourId as integers
        const nodeId = parseInt(edge[0])
        const neighbourId = parseInt(edge[1])
    
        if (!nodes.has(nodeId)) {
            nodes.set(nodeId, [])
        }
    
        if (!nodes.has(neighbourId)) {
            nodes.set(neighbourId, [])
        }
    
        // add neighbours to the nodes
        nodes.get(nodeId).push(neighbourId)
        nodes.get(neighbourId).push(nodeId)
    })
    
    return(nodes)
}

// export the function
module.exports = {
    parseFile
}