// READ INPUT FROM FILE

const fileparser = require('./utils/fileparser')

// parse file and return a map of nodes
const nodes = fileparser.parseFile('./input/test09.txt')

// calculate the max distance in graph for each node
let worstDistance

// for node 1 the max distance is 4
function propagate (nodeId, sourceId = null, distance = 0) {    
    const node = nodes.get(nodeId)

    // visit neighbours
    node.forEach(neighbourId => {
        if (neighbourId !== sourceId) {
            currentMinMaxDistance = distance + 1
            propagate(neighbourId, nodeId, distance + 1)
        }
    })
    
    if (distance > worstDistance)
        worstDistance = distance

    return
}


// test each node
let resultId = null
let resultDist = null

nodes.forEach((value, key) => {
    worstDistance = 0
    propagate(key)

    if (!resultDist) {
        resultDist = worstDistance
        resultId = key
    } else if (worstDistance < resultDist) {
        resultDist = worstDistance
        resultId = key
    }
})


// VISUALIZE NODES IN A GRAPH USING VEGA
const vega = require('./utils/visualize')
vega.visualize(nodes, resultId)

console.log(resultDist)