// READ INPUT FROM FILE
const fileparser = require('./utils/fileparser')

// parse file and return a map of nodes
const nodes = fileparser.parseFile('./input/test09.txt')



// process the queue
// pick the first node from the queue
// push its neighbours to the queue
// set the distance of the neighbours to the distance of the current node + 1
// repeat until the queue is empty
// return the maximum distance

function processQueue(startId) {
    // create a queue, push the first node and set its distance to 0
    const queue = []

    queue.push({
        nodeId: startId,
        sourceId: null,
        distance: 0
    })

    let worstDistance = 0

    let i = 10
    while (queue.length > 0) {
        const current = queue.shift()

        const nodeId = current.nodeId
        const distance = current.distance
        const sourceId = current.sourceId

        const node = nodes.get(nodeId)

        node.forEach(neighbourId => {
            // push neighbour to the queue
            // if neighbour is not the source node
            if (neighbourId !== sourceId) {
                queue.push({
                    nodeId: neighbourId,
                    sourceId: nodeId,
                    distance: distance + 1
                })
            }
        })

        // update worst distance
        if (distance > worstDistance)
            worstDistance = distance
    }

    return worstDistance
}

// find the lowest distance
let lowestDistance = Number.MAX_SAFE_INTEGER

// find the node with the lowest distance
let resultId = null

nodes.forEach((value, key) => {
    const distance = processQueue(key)

    if (distance < lowestDistance) {
        lowestDistance = distance
        resultId = key
    }
        
})

// VISUALIZE NODES IN A GRAPH USING VEGA
// const vega = require('./utils/visualize')
// vega.visualize(nodes, resultId)

console.log(lowestDistance)



