// READ INPUT FROM FILE
const fileparser = require('./utils/fileparser')

// parse file and return a map of nodes
const graph = fileparser.parseFile('./input/test09.txt')
// const visGraph = fileparser.parseFile('./input/test02.txt')
// console.log(graph)

// find the center node by removing all leaves

function findCenter(graph) {
    const queue = []
    
    let distance = 0

    while (graph.size > 2) {
        // find all leaves
        graph.forEach((node, key) => {
            if (node.length === 1) {
                queue.push(key)
            }
        });

        // remove leaves
        while (queue.length > 0) {
            const leaf = queue.shift()
            const parent = graph.get(leaf)[0]
            graph.get(parent).splice(graph.get(parent).indexOf(leaf), 1)
            graph.delete(leaf)
        }

        distance++
    }

    return {
        distance,
        resultId: graph.keys().next().value
    }
}

const { distance, resultId } = findCenter(graph)
console.log(distance, resultId)


// VISUALIZE NODES IN A GRAPH USING VEGA
// const vega = require('./utils/visualize')
// vega.visualize(visGraph, resultId)


